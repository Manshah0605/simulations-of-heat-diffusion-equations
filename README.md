# Simulations of heat diffusion equations

We will look at how the heat diffusion equation will be used for the diffusion of heat in different solids and also by changing different parameters we will analyse the equation's results.